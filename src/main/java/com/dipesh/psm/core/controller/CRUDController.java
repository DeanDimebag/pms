/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.psm.core.controller;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Dipesh
 */
public abstract class CRUDController<T, ID> extends SiteController {

    @Autowired
    public JpaRepository<T, ID> repository;
    
//for fetch
    @GetMapping
    public String index(Model model) {
        model.addAttribute("records", repository.findAll());
        return viewPath + "/index";
    }
    
//For create
    @GetMapping(value = "/create")
    public String create() {
        return viewPath + "/create";
    }
    
//For Save
    @PostMapping
    @Transactional
    public String save(T model) {
        repository.save(model);
        return "redirect:/" + viewPath + "?success";
    }
    @PostMapping(value = "/json")
    @Transactional
    @ResponseBody
    public String saveJson(T model) {
        repository.save(model);
        return "success";
    }
    
//For edit
    @GetMapping(value = "/edit/{id}")
    public String edit(@PathVariable("id") ID id, Model model) {
        model.addAttribute("record", repository.findById(id).get());
        return viewPath + "/edit";
    }
    
//For delete
    @GetMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id") ID id) {
        repository.deleteById(id);
        return "redirect:/" + viewPath + "?success";
    }
    
//For json
    @GetMapping(value = "/json")
    @ResponseBody
    public List<T> json() {
        return repository.findAll();
    }

    @GetMapping(value = "/json/{id}")
    @ResponseBody
    public T jsonDetail(@PathVariable("id") ID id) {
        return repository.findById(id).get();
    }
    
    @GetMapping (value = "/table")
    public String table(Model model) {
        model.addAttribute("records", repository.findAll());
        return viewPath + "/table";
    }
}
