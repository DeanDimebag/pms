/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.psm.core.controller;

import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author Dipesh
 */
public abstract class SiteController {

    protected String pageTitle;
    protected String pageURI;
    protected String viewPath;
    
    @ModelAttribute(value="pageTitle")
    public String getPageTitle() {
        return pageTitle;
    }
    
    @ModelAttribute(value="pageURI")
    public String getPageURI() {
        return pageURI;
    }

    @ModelAttribute(value="viewPath")
    public String getViewPath() {
        return viewPath;
    }
}
