/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.psm.repository;

import com.dipesh.psm.entity.Project;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Dipesh
 */
public interface ProjectRepository extends JpaRepository<Project, Integer>{
    
    @Query(value = "select * from tbl_projects order by created_date desc limit 4", nativeQuery = true)
    public List<Project> getRecentProject();
    
}
