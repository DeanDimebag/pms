/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.psm.repository;

import com.dipesh.psm.entity.Employee;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Dipesh
 */
public interface EmployeeRepository extends JpaRepository<Employee, Integer>{
    @Query(value = "select * from tbl_employee where id not in (select employee_id "
            + " from tbl_project_employees where project_id=?)", nativeQuery = true)
    public List<Employee> getEmployeeNotInProject(int projectId);
    
    @Query(value = "select * from tbl_employee order by created_date desc limit 4", nativeQuery = true)
    public List<Employee> getRecentEmployee();
}
