/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.psm.repository;

import com.dipesh.psm.entity.ProjectEmployee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Dipesh
 */
public interface ProjectEmployeeRepository extends JpaRepository<ProjectEmployee, Integer> {

    int deleteByProjectIdAndEmployeeId(int projectId, int employeeId);

//    @Query(value = "insert into tbl_project_employees (project_id, employee_id) values (?,?)",  nativeQuery = true)
//    public int insertByProjectIdAndEmployeeId(int projectId, int employeeId);
}
