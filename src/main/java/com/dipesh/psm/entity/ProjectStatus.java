/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.psm.entity;

import com.dipesh.psm.entity.master.MasterProjectStatus;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

/**
 *
 * @author Dipesh
 */
@Entity
@Table (name = "tbl_project_status")
public class ProjectStatus extends MasterEntity{
    @JoinColumn(name = "project_id", referencedColumnName = "id")
    @ManyToOne
    private Project project;
    
    @JoinColumn (name = "status_id", referencedColumnName = "id")
    @ManyToOne
    private MasterProjectStatus status;
    
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    
    @Type(type = "text")
    @Column(name = "remark")
    private String remark;

    public ProjectStatus() {
    }

    public ProjectStatus(Project project, MasterProjectStatus status, Date createdDate, String remark) {
        this.project = project;
        this.status = status;
        this.createdDate = createdDate;
        this.remark = remark;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public MasterProjectStatus getStatus() {
        return status;
    }

    public void setStatus(MasterProjectStatus status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    
    
}
