/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.psm.controller.master;

import com.dipesh.psm.core.controller.CRUDController;
import com.dipesh.psm.entity.master.MasterProjectStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping(value = "/master/project-status")
public class MasterProjectStatusController extends CRUDController<MasterProjectStatus, Integer> {

    public MasterProjectStatusController() {
        pageTitle = "Project Status";
        pageURI = "master/project-status";
        viewPath = "master/project_status";
    }
}
