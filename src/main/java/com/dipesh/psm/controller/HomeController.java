/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.psm.controller;

import com.dipesh.psm.core.controller.SiteController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping (value = "/")
public class HomeController extends SiteController{
    @GetMapping
    public String index(){
        return "index";
    }

    public HomeController() {
        pageTitle = "PMS";
    }
}
