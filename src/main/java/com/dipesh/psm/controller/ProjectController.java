/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.psm.controller;

import com.dipesh.psm.core.controller.CRUDController;
import com.dipesh.psm.entity.Employee;
import com.dipesh.psm.entity.Project;
import com.dipesh.psm.entity.ProjectEmployee;
import com.dipesh.psm.entity.ProjectStatus;
import com.dipesh.psm.entity.master.MasterProjectStatus;
import com.dipesh.psm.repository.EmployeeRepository;
import com.dipesh.psm.repository.MasterProjectStatusRepository;
import com.dipesh.psm.repository.ProjectEmployeeRepository;
import com.dipesh.psm.repository.ProjectRepository;
import com.dipesh.psm.repository.ProjectStatusRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping(value = "/projects")
public class ProjectController extends CRUDController<Project, Integer> {

    @Autowired
    private ProjectStatusRepository projectStatusRepository;
    
    @Autowired
    private ProjectRepository repository;

    @Autowired
    private ProjectEmployeeRepository projectEmpRepository;

    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Autowired
    private MasterProjectStatusRepository masterProjectStatusRepository;

    public ProjectController() {
        viewPath = "projects";
        pageTitle = "Project";
        pageURI = "projects";
    }

    @GetMapping(value = "/employees/{projectId}")
    @ResponseBody
    public List<Employee> getProjectEmployeejson(
            @PathVariable("projectId") int projectId) {
        Project project = repository.findById(projectId).get();
        return project.getEmployees();
    }

    @PostMapping(value = "/remove-employee")
    @ResponseBody
    @Transactional
    public String removeEmployee(@RequestParam("projectId") int projectId,
            @RequestParam("employeeId") int employeeId) {
        projectEmpRepository.deleteByProjectIdAndEmployeeId(projectId, employeeId);
        return "Employee ID : " + employeeId + ",Project ID : " + projectId;
    }

    @GetMapping(value = "/unselect-employees/{projectId}")
    @ResponseBody
    public List<Employee> getUnselectEmployeeJson(@PathVariable("projectId") int projectId) {
        return employeeRepository.getEmployeeNotInProject(projectId);
    }

    @PostMapping(value = "/add-employee")
    @ResponseBody
    @Transactional
    public String addEmployee(ProjectEmployee projectEmployee) {
        projectEmpRepository.save(projectEmployee);
        return projectEmployee.toString();
    }
    
    @GetMapping(value = "/status/{id}")
    @ResponseBody
    public List<MasterProjectStatus> getStatusUnselect(@PathVariable("id")int projectId){
        return masterProjectStatusRepository.getStatusNotInProject(projectId);
    }
    
    @PostMapping(value = "/add-status")
    @ResponseBody
    public String addStatus(ProjectStatus projectStatus){
        projectStatusRepository.save(projectStatus);
        return projectStatus.toString();
    }
    
    @GetMapping(value = "/recent-project")
    @ResponseBody
    public List<Project> getRecentProjectList(){
        return repository.getRecentProject();
    }
}
