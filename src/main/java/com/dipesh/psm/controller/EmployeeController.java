/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.psm.controller;

import com.dipesh.psm.core.controller.CRUDController;
import com.dipesh.psm.entity.Employee;
import com.dipesh.psm.repository.EmployeeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping(value = "/employees")
public class EmployeeController extends CRUDController<Employee, Integer> {
    
    @Autowired
    private EmployeeRepository employeeRepository;

    public EmployeeController() {
        viewPath = "employees";
        pageTitle = "Employee";
        pageURI = "employees";
    }
    
    
    @GetMapping (value = "/recent-employee")
    @ResponseBody
    public List<Employee> getRecentEmployee(){
        return employeeRepository.getRecentEmployee();
    }
}
