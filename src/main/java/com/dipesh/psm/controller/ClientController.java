/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.psm.controller;

import com.dipesh.psm.core.controller.CRUDController;
import com.dipesh.psm.entity.Client;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping(value = "/clients")
public class ClientController extends CRUDController<Client, Integer> {

    public ClientController() {
        viewPath = "clients";
        pageTitle = "Client";
        pageURI = "clients";
    }

    @PostMapping(value = "/status")
    @Transactional
    @ResponseBody
    public String updateStatus(@RequestParam("id") int id,
            @RequestParam("status") boolean status) {
            Client client = repository.findById(id).get();
            client.setStatus(status);
            repository.save(client);
            return "success";
    }
}
